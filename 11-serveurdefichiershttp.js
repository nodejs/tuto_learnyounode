const port = process.argv[2];
const filePath = process.argv[3];

const http = require('http');
const fs = require('fs');

let server = http.createServer(function (request, response) {
    let stream = fs.createReadStream(filePath);
    stream.on('open', function() {
        stream.pipe(response);
    });
    stream.on('error', function (err) {
        response.end(err);
    });
});
server.listen(port);