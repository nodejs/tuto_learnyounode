const argv = process.argv;

const http = require('./Http');

function info(data) {
    console.log(data);
}
function error(err) {
    console.error("Une erreur s'est produite : " + err);
}

function get(url, callback) {
    http.get(url, function (err, data) {
        if (err) {
            return error(err);
        }
        callback(null, data)
    });
}

get(argv[2], function (err, data) {
    info(data);
    get(argv[3], function (err, data) {
        info(data);
        get(argv[4], function (err, data) {
            info(data);
        });
    });
});