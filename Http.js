const http = require ('http');
const bl = require('bl');


var exports = module.exports = {};
exports.get = function (url, callback) {
    http.get(url, function(response) {
        response.pipe(bl(function(err, data) {

            if (err) {
                return callback(err);
            }
            return callback(null, data.toString())
        }));
    }).on('error', function(error) {
        return callback(error);
    })
};