var http = require ('http');

module.exports = function (url, callback) {
    http.get(url, function(response) {

        response.setEncoding('utf8');

        response.on("error", function(error) {
            return callback(error);
        });

        response.on("data", function(data) {
            return callback(null, data);
        });
    }).on('error', function(error) {
        return callback(error);
    })
};