var fs = require('fs');
var path = require('path');

module.exports = function(dirPath, extensionFilter, callback) {

    fs.readdir(dirPath, function(err, files) {
        if (err) {
            return callback(err);
        }

        let filteredFiles = [];
        files.forEach(function(file) {
            if (path.extname(file) === '.' + extensionFilter) {
                filteredFiles.push(file);
            }
        });
        return callback(null, filteredFiles);
    });
};