const port = process.argv[2];

const http = require('http');
const url = require('url');
const api = require('./Api');

let server = http.createServer(function (request, response) {

    if (request.method !== 'GET') {
        return response.end("Ce n'est pas une requête GET !");
    }

    api.init(request, response);
    const parsedUrl = url.parse(request.url, true);
    const json = api.get(parsedUrl.pathname, parsedUrl.query);

    if (!json) {
        response.writeHead(404);
    } else {
        response.writeHead(200, {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*"
        });
        response.write(JSON.stringify(json));
    }
    response.end();
});
server.listen(port);