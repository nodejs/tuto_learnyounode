const net = require('net');
const strftime = require('strftime');

module.exports = {
    init: function (port) {
        let server = net.createServer(function (socket) {
            socket.write(strftime('%Y-%m-%d %H:%M') + '\n');
            socket.end();
        });
        server.listen(port);
    }
};