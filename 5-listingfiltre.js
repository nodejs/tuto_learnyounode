var directoryPath = process.argv[2];
var extension = process.argv[3];

var fs = require('fs');

fs.readdir(directoryPath, function(err, files) {
    if (err) {
        return console.error(err);
    }

    for (let i = 0; i < files.length; i++) {
       if (files[i].endsWith('.' + extension)) {
           console.log(files[i])
       }
    }
});