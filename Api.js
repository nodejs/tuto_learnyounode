module.exports = function () {

    let request = null;
    let response = null;

    let route = function(path, parameters) {
        switch (path) {
            case '/api/parsetime':
                if (!parameters.iso) {
                    return console.error('Paramètre "iso" manquant !');
                }
                return parsetime(parameters.iso);

            case '/api/unixtime':
                if (!parameters.iso) {
                    return console.error('Paramètre "iso" manquant !');
                }
                return unixtime(parameters.iso);

            default :
                return console.error('Route inconnue : "' + path + '" !');
        }
    };

    let parsetime = function(dateIso) {

        let date = new Date();
        date.setTime(Date.parse(dateIso));

        return {
            "hour": date.getHours(),
            "minute": date.getMinutes(),
            "second": date.getSeconds()
        }
    };

    let unixtime = function(dateIso) {
        return {
            "unixtime": Date.parse(dateIso)
        }
    };

    return {
        init: function(req, resp) {
            request = req;
            response = resp;
        },

        get: function(path, parameters) {
            return route(path, parameters);
        }
    }
}();