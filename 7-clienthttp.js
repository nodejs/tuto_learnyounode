var url = process.argv[2];

var httpGetFn = require('./HttpGet');

httpGetFn(url, function (err, data) {
    if (err) {
        return console.error("Une erreur s'est produite : " + err);
    }

    console.log(data);
});